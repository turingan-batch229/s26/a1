Activity

Create sample documents following the models we have created for users and courses for our booking-system.

user1 and user2 are the ids for the user documents.
course1 and course2 are the ids for the course documents.

user1 is enrolled in course1.
user2 is enrolled in course1 and course2.

2 Model Booking System with Embedding

user1 {

	"id": "student1",
	"firstName": "James",
	"lastName": "Uy",
	"email": "jamesuy12@gmail.com",
	"password": "juy1234",
	"mobileNumber": "09603411901",
	"isAdmin": false,
	"enrollments": [
		{

			"id": "enroll1",
			"courseId": "program1",
			"courseName": "PHP.net",
			"dateEnrolled": "23/08/2023"
		}
	]
}
user2 {

	"id": "student2",
	"firstName": "RJ",
	"lastName": "Torres",
	"email": "rtorres12@gmail.com",
	"password": "rt1234",
	"mobileNumber": "09210266326",
	"isAdmin": false,
	"enrollments": [
		{

			"id": "enroll1",
			"courseId": "program1",
			"courseName": "PHP.net",
			"dateEnrolled": "23/08/2023"
		},
		{

			"id": "enroll2",
			"courseId": "program2",
			"courseName": "COBOL",
			"dateEnrolled":"30/09/2023"
		}
	]

}


course1 {

	"id": "program1",
	"name": "PHP.net",
	"description": "Scripting Language",
	"price": 15000,
	"instructor": "Rome",
	"isActive": true,
	"enrollees": [

		{
			"id": "enrollee1",
			"userId": "student1",
			"fullName": "James Uy",
			"dateEnrolled": "23/08/2023"
		},
		{
			"id": "enrollee2",
			"userId": "student2",
			"fullName": "RJ Torres",
			"dateEnrolled": "30/09/2023"
		}

	]

}
course2 {

	"id": "program2",
	"name": "COBOL",
	"description": "Programming Laguage for business",
	"price": 20000,
	"instructor": "Mike",
	"isActive": true,
	"enrollees": [

		{
			"id": "enrollee2",
			"userId": "student2",
			"fullName": "RJ Torres",
			"dateEnrolled": "30/09/2023"
		}

	]

}